# misMod WorkSpace

This workspace is Designed to Help with Miscreated Mod Development, Featuring Lua Intellisense, Formatting and CodeCompleation for many CE3 & Miscreated Lua Functions
---

## Includes:

* CE3 & Micscreated API included for Intellisense Support (found in workspace/libs)
    - Partialy Documented (to be improved)

* A bunch of Lua/QoL Related VSCode Extensions included In the Workspace Recommendations.

* Basic Miscreated Modding Tips and Links to Related Content

Youll need to go to the VSCode Extensions Screen and Show Recommended and install the Required Extensions for this to work properly.

![Enable Recommended Extensions](files\vscode_recommended.png)


You can then make a copy of this Blank workspace and its files, then Rename the `misModWorkspace.code-workspace` file to yourMod`.code-workspace` to get started.

---

## Resources

    - Nothing here Yet

## Links:

* Discord:

    - Official Miscreated Discord: [Join Here](https://discord.gg/Y3KzabU)

    - UnOfficial Modding Discord: [Join Here](https://discord.gg/utNk4Dy)

* Lua Documentation:

    - [Official](https://www.lua.org/manual/5.1/) (Official Lua5.1 Manual)

    - [DevDocs](https://devdocs.io/lua~5.1/) (Faster , Easier Reference)